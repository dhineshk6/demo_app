package com.genpact.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import com.genpact.demo.configuration.JpaConfiguration;
import com.genpact.demo.model.User;
import com.genpact.demo.repositories.UserRepository;


@Import(JpaConfiguration.class)
@SpringBootApplication(scanBasePackages={"com.genpact.demo"})// same as @Configuration @EnableAutoConfiguration @ComponentScan
public class SpringBootCRUDApp {

	private static final Logger logger = LoggerFactory.getLogger(SpringBootCRUDApp.class);
	public static void main(String[] args) {
		SpringApplication.run(SpringBootCRUDApp.class, args);
	}
	
	@Bean
	public CommandLineRunner setup(UserRepository userRepository) {
		return (args) -> {
			userRepository.save(new User("Sam",11, 11000 ));
			userRepository.save(new User("Tom",22, 22000));
			userRepository.save(new User("Tim",33, 33000 ));
			userRepository.save(new User("Kim",44, 44000));
			
			logger.info("The sample data has been generated");
		};
	}
}
